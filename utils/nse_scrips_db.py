from nsetools import Nse
from googlefinance.client import get_price_data, get_prices_data, get_prices_time_data
from pymodm import connect, EmbeddedMongoModel, MongoModel, fields
from pymodm.errors import ValidationError, ConfigurationError
from pymongo.write_concern import WriteConcern
import pprint

class stock_time_data(EmbeddedMongoModel):
    timestamp = fields.DateTimeField() # timestamp
    Open = fields.FloatField()
    Close = fields.FloatField()
    High = fields.FloatField()
    Low = fields.FloatField()
    Volume = fields.FloatField()

class nse_stock(MongoModel):
    extremeLossMargin = fields.FloatField()
    cm_ffm = fields.FloatField(blank = True)
    bcStartDate = fields.CharField(blank = True)
    change = fields.CharField(blank = True)
    buyQuantity3 = fields.FloatField(blank = True)
    sellPrice1 = fields.FloatField(blank = True)
    buyQuantity4 = fields.FloatField(blank = True)
    sellPrice2 = fields.FloatField(blank = True)
    priceBand = fields.CharField(blank = True)
    buyQuantity1 = fields.FloatField(blank = True)
    deliveryQuantity = fields.FloatField(blank = True)
    buyQuantity2 = fields.FloatField(blank = True)
    sellPrice5 = fields.FloatField(blank = True)
    quantityTraded = fields.FloatField(blank = True)
    buyQuantity5 = fields.FloatField(blank = True)
    sellPrice3 = fields.FloatField(blank = True)
    sellPrice4 = fields.FloatField(blank = True)
    low52 = fields.FloatField(blank = True)
    securityVar = fields.FloatField(blank = True)
    marketType = fields.CharField(blank = True)
    pricebandupper = fields.FloatField(blank = True)
    totalTradedValue = fields.FloatField(blank = True)
    faceValue = fields.FloatField(blank = True)
    ndStartDate = fields.CharField(blank = True)
    previousClose = fields.FloatField(blank = True)
    symbol = fields.CharField(blank = True)
    varMargin = fields.FloatField(blank = True)
    lastPrice = fields.FloatField(blank = True)
    pChange = fields.FloatField(blank = True)
    adhocMargin = fields.FloatField(blank = True)
    companyName = fields.CharField(blank = True)
    averagePrice = fields.FloatField(blank = True)
    secDate = fields.CharField(blank = True) # TODO
    series = fields.CharField(blank = True)
    isinCode = fields.CharField(blank = True)
    surv_indicator = fields.CharField(blank = True)
    indexVar = fields.CharField(blank = True)
    pricebandlower = fields.FloatField(blank = True)
    totalBuyQuantity = fields.FloatField(blank = True)
    high52 = fields.FloatField(blank = True)
    purpose = fields.CharField(blank = True)
    cm_adj_low_dt = fields.CharField(blank = True)
    closePrice = fields.FloatField(blank = True)
    isExDateFlag = fields.FloatField(blank = True)
    recordDate = fields.CharField(blank = True)
    cm_adj_high_dt = fields.CharField(blank = True) # TODO
    totalSellQuantity = fields.FloatField(blank = True)
    dayHigh = fields.FloatField(blank = True)
    exDate = fields.CharField(blank = True) # TODO
    sellQuantity5 = fields.FloatField(blank = True)
    bcEndDate = fields.DateTimeField(blank = True)
    css_status_desc = fields.CharField(blank = True)
    ndEndDate = fields.CharField(blank = True)
    sellQuantity2 = fields.FloatField(blank = True)
    sellQuantity1 = fields.FloatField(blank = True)
    buyPrice1 = fields.FloatField(blank = True)
    sellQuantity4 = fields.FloatField(blank = True)
    buyPrice2 = fields.FloatField(blank = True)
    sellQuantity3 = fields.FloatField(blank = True)
    applicableMargin = fields.FloatField(blank = True)
    buyPrice4 = fields.FloatField(blank = True)
    buyPrice3 = fields.FloatField(blank = True)
    buyPrice5 = fields.FloatField(blank = True)
    dayLow = fields.FloatField(blank = True)
    deliveryToTradedQuantity = fields.FloatField(blank = True)
    basePrice = fields.FloatField(blank = True)
    totalTradedVolume = fields.FloatField(blank = True)
    dayopen = fields.FloatField(blank = True)

    #  time_data = fields.EmbeddedDocumentListField(stock_time_data)


    class Meta:
        write_concern = WriteConcern(j=True)
        connection_alias = 'paisa_picker'

def db_connect(): 
    try:
        connect("mongodb://localhost:27017/paisa-picker", alias="paisa_picker")
    except:
        print("Failed to connect to database")
        exit()

def nse_connect():
    global nse 
    nse = Nse()

def all_stock_symbols_save():
    stock_codes = nse.get_stock_codes()
    del stock_codes['SYMBOL']

    db_stocks = db.stocks

    for k in stock_codes:
        print(k)

    # db_stocks.insert_many([{"symbol": k, "name": stock_codes[k], "exchange" : "NSE"} 
    #                         for k in stock_codes if stock_codes[k] is not "SYMBOL"])
    # spprint.pprint(db_stocks)

def stock_historical_data_get(stock_symbol, interval=10, period="6Y"):
    exchange = 'NSE'
    print(stock_symbol)
    params = {'q': stock_symbol, 'x': exchange, 'i': interval, 'p': period}
    return get_price_data(params)

def stock_time_data_save():
    # db = client.paisa_picker
    # db_stocks = db.stocks
    # pprint.pprint(db_stocks)
    # for stock in db_stocks.find():
    q = nse.get_quote('WIPRO')
    val = q['open']
    q['dayopen'] = val
    del q['open']
    print(q)
    print(type(q))
    nse_stock.from_document(q).save()
    # df = stock_historical_data_get('WIPRO', interval=86400, period="1Y")

    # for index, row in df.iterrows():
    #    pprint.pprint(row['Open'])
   
db_connect()
nse_connect()
stock_time_data_save()

# client.close()